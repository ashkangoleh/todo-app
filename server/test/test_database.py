import pytest
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from unittest.mock import Mock,patch
import pytest
from src.helpers import logger
from src.db.database import DataBaseManager, DataBaseErrorHandling
from src.db import Session

def test_engine_property():
    engine_mock = Mock()

    with patch("sqlalchemy.create_engine", engine_mock):

        assert DataBaseManager().engine != engine_mock



def test_session_property():
    with DataBaseManager() as db:
        assert isinstance(db, Session)


def test_context_manager():

    session_mock = Mock()
    session_close_mock = Mock()
    session_mock.close = session_close_mock

    with DataBaseManager() as session:
        assert session != session_mock


def test_db_manager_with_connection_error():
    logger_mock = Mock()
    logger.error = logger_mock
    create_engine_mock = Mock(side_effect=Exception)

    with patch("sqlalchemy.create_engine", create_engine_mock):
        with pytest.raises(DataBaseErrorHandling) as e:
           yield e

    logger_mock.error.assert_called_once_with("Connection refused: Connection refused")