from main import app
from unittest.mock import patch
from fastapi.testclient import TestClient
import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def test_create_user():
    client = TestClient(app)
    user_data = {
        "email": "test@example.com",
        "password": "password123",
        "password2": "password123"
    }
    with patch("src.api.routes.authentication.user_route") as mock_create_user:
        mock_create_user.return_value = user_data
        response = client.post("/api/v1/user/create", json=user_data)

        assert response.status_code == 200
        assert response.json() == {
            "email": "test@example.com",
            "id": 1
        }
