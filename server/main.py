"""
Main file to Run application
"""

from src.settings.app_handlers import app, APP_SETTING
from src.settings.routers import Routers
import uvicorn as UV

ROUTES = [
    'src.api.routes.user_route',
    'src.api.routes.auth_route',
    'src.api.routes.category_route',
    'src.api.routes.task_route',
]

routers = Routers(app, ROUTES)()

if __name__ == "__main__":
    if APP_SETTING.DEBUG:
        UV.run(
            "main:app",
            reload=APP_SETTING.RELOAD,
            port=APP_SETTING.APPLICATION_PORT,
        )
    # Uvicorn run application by running main.py
    UV.run(
        "main:app",
        reload=APP_SETTING.RELOAD,
        host="0.0.0.0",
        port=APP_SETTING.APPLICATION_PORT,
        limit_concurrency=APP_SETTING.LIMIT_CONCURRENCY,
        limit_max_requests=APP_SETTING.LIMIT_MAX_REQUESTS,
        backlog=APP_SETTING.BACKLOG,
        workers=APP_SETTING.WORKERS,
    )
