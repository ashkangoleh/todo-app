"""
Session generator to yield session
it'll use for ORM or Pure query from sqlalchemy
"""
from .database import DataBaseManager


def get_db():
    """
    generator to get session
    """
    with DataBaseManager() as db:
        yield db
