"""
Database Manager include
Engine, session and database error handler
"""
from dataclasses import dataclass
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from src.settings import DB_SETTING
from sqlalchemy.orm import sessionmaker, Session
from src.helpers import logger


class SingletonDBManager(type):
    """
    Singleton database manager mixin
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]


class DataBaseErrorHandling(Exception):
    """
    Database Error handling for custom raise
    """
    pass


@dataclass
class DataBaseManager(metaclass=SingletonDBManager):
    """
    Manages database operations
    """
    _engine = None
    _sessionmaker = None

    @property
    def engine(self):
        """
        SQLAlchemy engine instance.

        Returns:
            sqlalchemy.engine.Engine: A SQLAlchemy engine instance.

        Raises:
            DataBaseErrorHandling: throw error while creating engine.

        """
        try:
            if self._engine is None:
                self._engine = create_engine(
                    f"{DB_SETTING.DATABASE_DRIVER}"
                    f"://{DB_SETTING.DATABASE_USER}:"
                    f"{DB_SETTING.DATABASE_PASSWORD}@"
                    f"{DB_SETTING.DATABASE_URL}:"
                    f"{DB_SETTING.DATABASE_PORT}/"
                    f"{DB_SETTING.DATABASE_DB}",
                    pool_size=DB_SETTING.POOL_SIZE,
                    max_overflow=DB_SETTING.MAX_OVERFLOW,
                    pool_recycle=DB_SETTING.POOL_RECYCLE,
                    pool_timeout=DB_SETTING.POOL_TIMEOUT,
                    echo=DB_SETTING.DEBUG,
                )
            return self._engine
        except OperationalError as e:
            logger.error(f"{e}")

    @property
    def session(self) -> Session:
        """
        Returns a sessionmaker instance with specified options.

        Returns:
            sqlalchemy.orm.session.sessionmaker: A sessionmaker instance.

        """
        if self._sessionmaker is None:
            self._sessionmaker = sessionmaker(
                autocommit=False,
                autoflush=False,
                expire_on_commit=False,
                bind=self.engine,
            )
        return self._sessionmaker()

    def __enter__(self):
        """opening contextmanager by enter magic method

        Returns:
            self.session: while open instantiated class with context manager
            session is open to use
        """
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        """closing contextmanager by exit magic method

        Returns:
            self.session.close: while close
                instantiated class with context manager
                session will close
        """
        self.session.close()


try:
    logger.info("Connecting to database...")
    engine = DataBaseManager().engine
except OperationalError as e:
    logger.error(f"Connection refused: {str(e)}")
    raise DataBaseErrorHandling(f"Error creating sync engine: {e}") from e
