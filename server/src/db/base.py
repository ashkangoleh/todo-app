"""
Base declarative extention
"""
from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    pass
