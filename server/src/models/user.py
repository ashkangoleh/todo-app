"""
Represents a User in the application.
"""
from sqlalchemy.orm import relationship
from src.db import Base
from sqlalchemy import Column, Integer, String, DateTime, Boolean


# User Model
class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    password = Column(String, nullable=False)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, server_default="NOW()")
    last_login = Column(DateTime)

    tasks = relationship("Task", back_populates="owner")
    categories = relationship("Category", back_populates="owner")
