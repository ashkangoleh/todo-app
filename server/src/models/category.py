"""
Represents a Category in the application.
"""
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey
from src.db import Base


# Category Model
class Category(Base):
    __tablename__ = "categories"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(String, nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"))

    tasks = relationship(
        "Task", back_populates="category",
        cascade="all, delete")
    owner = relationship("User", back_populates="categories")
