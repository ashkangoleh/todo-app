from .task import Task # noqa
from .user import User # noqa
from .category import Category # noqa
from src.db import Base, engine
from src.settings import DB_SETTING


def init_db():
    if DB_SETTING.DROP_TABLE:
        Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
