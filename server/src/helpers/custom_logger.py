"""
Custom logger (loguru is provider)
i'll clean stdout.log after 2 days

for develop mode uncomment "file" and add into format string
"""
import sys
from loguru import logger
from src.settings import APP_SETTING


logger.add(
    sys.stderr,
    format="<red>[{level}]</red> | Message"
    ": <green>{message}</green> @ {time}",
    colorize=True, level=APP_SETTING.LOG_LEVEL
)


logger.add("stdout.log", retention="2 days")  # clean after 2 days
