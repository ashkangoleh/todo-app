"""
Sqlalchemy model decoder as list
reading Objects from Sqlalchemy and decode it to a list
"""


def model_jsonable_decoder(objs):
    import datetime
    from sqlalchemy.orm import class_mapper
    """Converts a list of SQLAlchemy objects to a list of dictionaries."""
    if not objs:
        return None
    if not isinstance(objs, list):
        mapper = class_mapper(objs.__class__)
        columns = [column.key for column in mapper.columns]

        def get_key_value(c):
            return (c, getattr(obj, c).isoformat() if isinstance(
                getattr(obj, c), datetime.datetime) else getattr(obj, c))
        return dict(map(get_key_value, columns))
    dicts = []
    for obj in objs:
        mapper = class_mapper(obj.__class__)
        columns = [column.key for column in mapper.columns]

        def get_key_value(c):
            return (c, getattr(obj, c).isoformat() if isinstance(
                getattr(obj, c), datetime.datetime) else getattr(obj, c))
        dicts.append(dict(map(get_key_value, columns)))
    return dicts
