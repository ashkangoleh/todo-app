"""
Routers handler by include routes as a element
append dynamically routes
*router must be correct path
"""
from typing import Any
from fastapi import FastAPI


class Routers:
    """
    A utility class for registering FastAPI routers.

    :param app: The FastAPI application instance.
    :type app: FastAPI
    :param routes: A list of the router modules to register.
    :type routes: list
    """

    def __init__(self, app: FastAPI, routes: list) -> None:
        self.app = app
        self.routes = routes

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        """
        Registers the routers by calling the route methods.

        :return: The instance of the class.
        """
        self._create_route_methods()
        return self

    def _create_route_methods(self):
        """
        Creates route methods for each router module,
        and registers them with the application.
        """
        for route in self.routes:
            module_path, route_name = route.rsplit('.', maxsplit=1)
            module = __import__(module_path, fromlist=[route_name])
            route_module = getattr(module, route_name)
            route_method_name = f'{route_name.title().replace("_", "")}Route'
            def route_method(): return self.app.include_router(route_module)
            setattr(self, route_method_name, route_method)
            getattr(self, route_method_name)()
