"""
Custom metadata for application.
"""
tags_metadata = [
    {
        "name": "User",
        "description": "The **Register** logic is also here.",
    },
    {
        "name": "Authentication",
        "description": "The **Authentication** logic is also here.",
        "externalDocs": {
            "description": "JWT token generator",
            "url": "https://jwt.io/",
        },
    },
    {
        "name": "Category",
        "description": "The **Category** CRUD operations are also here.",
    },
    {
        "name": "Tasks",
        "description": "The **Tasks** CRUD operations are also here.",
    },

]
description = """
ToDo list Application for testing Programming skills \U0001F680 \n
<hr>
LinkedIn \U0001F517:
    <a>https://www.linkedin.com/in/ashkan-goleh-pour-0463b8127/</a>\n
GitLab \U0001F517:
    <a>https://gitlab.com/ashkangoleh/todo-app.git</a>
<hr>
<p>For using swagger, replace '<strong>/docs</strong>'
    instead of the current path.</p>
"""
contact = {
    "name": "Ashkan Goleh Pour",
    "url": "https://devsteam.ir",
    "email": "Ashkangoleh@gmail.com",
}
