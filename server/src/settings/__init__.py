from .configs import auth_setting, database_setting, application_setting

DB_SETTING = database_setting()
APP_SETTING = application_setting()
AUTH_SETTING = auth_setting()
