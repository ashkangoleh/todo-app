"""
App Handler here to manage whole application
include middlewares , http exceptions
docs and redocs redirection from root path
"""
from fastapi import FastAPI
from fastapi.exceptions import FastAPIError
from starlette.responses import JSONResponse
from starlette.requests import Request
from starlette import status
from fastapi.exceptions import RequestValidationError
from src.api.exceptions import CustomAppException
from src.settings import APP_SETTING
from starlette.middleware.exceptions import ExceptionMiddleware
from src.models import init_db
from fastapi.openapi.docs import get_swagger_ui_html, get_redoc_html
from fastapi.responses import RedirectResponse
from src.helpers import logger
from src.settings.tags_metadata import tags_metadata, description, contact
from fastapi.middleware.trustedhost import TrustedHostMiddleware
from fastapi.middleware.cors import CORSMiddleware


def create_app() -> FastAPI:
    """
    Create app instance
    """
    try:
        current_app = FastAPI(title="ToDo List",
                              description=description,
                              version="0.0.1",
                              contact=contact,
                              debug=APP_SETTING.DEBUG,
                              docs_url=None,
                              redoc_url=None,
                              openapi_tags=tags_metadata,
                              )
        return current_app
    except FastAPIError as e:
        logger.error(f"{str(e)}")


app = create_app()


@app.exception_handler(CustomAppException)
async def custom_exception_handler(request: Request, exc: CustomAppException):
    """
    Defines an exception handler for CustomAppException for the application.

    Parameters:
    -----------
    request (Request):
        The request object received by the application.

    exc (CustomAppException):
        The instance of CustomAppException that is being handled.

    Returns:
    -----------
    A JSONResponse object containing error details for the client,
        including the error code and message from the exception instance.

    Raises:
    -----------
    CustomAppException:
        If there is any exception of type CustomAppException,
        it will be handled by this function and a JSON response
        will be returned containing the error code and message.

    """
    return JSONResponse(
        content={"code": exc.code,
                 "message": f"{exc.message}"},
    )


@app.exception_handler(
    RequestValidationError
)
async def validation_exception_handler(
    request: Request,
    exc: RequestValidationError
):
    """
    Defines an exception handler for
        RequestValidationError for the FastAPI application.

    Parameters:
    -----------
    request (Request):
        The request object received by the application.

    exc (RequestValidationError):
        The instance of RequestValidationError
            that is being handled.

    Returns:
    -----------
    A JSONResponse object containing error
        details for the client,
        including the error message and request
        body from the exception instance.
        The status code will be set to UNPROCESSABLE_ENTITY.

    Raises:
    -----------
    RequestValidationError:
        If there is any exception of type RequestValidationError,
        it will be handled by this function and a JSON response will be
        returned containing the error message and request body.
        The status code will be set to UNPROCESSABLE_ENTITY.
    """
    return JSONResponse(
        content={
            "msg": exc.errors()[0]['msg'], "body": exc.body
        },
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
    )

app.add_middleware(ExceptionMiddleware, handlers=app.exception_handlers)
app.add_middleware(
    CORSMiddleware,
    allow_origins=APP_SETTING.ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(
    TrustedHostMiddleware, allowed_hosts=APP_SETTING.ALLOWED_HOST,
)


@app.get("/docs", include_in_schema=False)
async def swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url="/openapi.json",
        title="Todo List",
    )


@app.get("/documentation", include_in_schema=False)
async def swagger_ui_html():
    return get_redoc_html(
        openapi_url="/openapi.json",
        title="Todo List"
    )


@app.get("/", include_in_schema=False, response_class=RedirectResponse)
async def docs_redirect() -> RedirectResponse:
    """docs redirector
    Description
    -----------
        While root path calling from client it'll redirect to redoc path
    Returns:
    -----------
        RedirectResponse: redirect to '/documentation' path
    """
    return RedirectResponse(url='/documentation')


@app.on_event('startup')
async def start_up_events():
    """Start up event listener to incoming callables
    """
    logger.info("Starting Application.")
    init_db()


@app.on_event('shutdown')
async def shutdown_events():
    """Shutdown event listener to exit application
    """
    logger.info("Application shutdown.")
