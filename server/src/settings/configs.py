"""
Configuration file to manage .env files and
"""
from pydantic import BaseSettings
import secrets
from pydantic.tools import lru_cache
from src import utils


class ApplicationSetting(BaseSettings):
    DEBUG: bool
    LOG_LEVEL: str
    WORKERS: int
    RELOAD: bool
    LIMIT_CONCURRENCY: int
    LIMIT_MAX_REQUESTS: int
    APPLICATION_PORT: int
    BACKLOG: int
    ALLOWED_HOST: utils.List[str]
    ORIGINS: utils.List[str]

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'


class DataBaseSetting(BaseSettings):
    """
    Stores settings related to database connection and connection pool.

    Attributes:
        DATABASE_URL (str):
            URL of the database.
        DATABASE_PORT (int):
            Port number for the database connection.
        DATABASE_USER (str):
            User name for the database connection.
        DATABASE_PASSWORD (str):
            Password for the database connection.
        DATABASE_DB (str):
            Name of the database to connect to.
        DATABASE_DRIVER (str):
            Driver to use for the database connection.
        POOL_SIZE (int):
            Maximum number of connections to keep in the pool.
        MAX_OVERFLOW (int):
            Maximum number of connections to allow in the overflow queue.
        POOL_RECYCLE (int):
            Time interval in seconds after which
            to recycle a connection.
        POOL_TIMEOUT (int):
            Timeout in seconds for acquiring
            a connection from the pool.
        DROP_TABLE (bool):
            Flag to indicate whether
            to drop all tables during database initialization.
        DEBUG (bool):
            Flag to indicate whether to enable verbose SQL logging.

    Note:
        The settings are loaded from a .env file
        in the current working directory.
        The environment variable names should match
        the attribute names in this class.
    """
    DATABASE_URL: str
    DATABASE_PORT: int
    DATABASE_USER: str
    DATABASE_PASSWORD: str
    DATABASE_DB: str
    DATABASE_DRIVER: str
    POOL_SIZE: int
    MAX_OVERFLOW: int
    POOL_RECYCLE: int
    POOL_TIMEOUT: int
    DROP_TABLE: bool
    DEBUG: bool

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'


class AuthSetting(BaseSettings):
    """
    Represents the authentication settings used by the application.

    Attributes:
        ACCESS_TOKEN_EXPIRE_MINUTES (int):
            The number of minutes after which an access token will expire.
        REFRESH_TOKEN_EXPIRE_MINUTES (int):
            The number of minutes after which a refresh token will expire.
        ALGORITHM (str):
            The name of the encryption algorithm used to sign tokens.
        SECRET_KEY (str):
            The secret key used to sign tokens and encrypt sensitive data.

    Configurations:
        env_file (str): The name of the file containing environment variables.
        env_file_encoding (str): The encoding used by the environment file.
    """
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 30  # 30 minutes
    REFRESH_TOKEN_EXPIRE_MINUTES: int = 10080  # 7 days
    ALGORITHM: str
    SECRET_KEY: str
    PASSWORD_SCHEMES: str

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.update_secret_key()

    def update_secret_key(self):
        """
        Updates the SECRET_KEY attribute with a new random
            hexadecimal string of 32 characters
            if the current time is midnight (00:00). Then,
            it reads the .env file to find the line
            that starts with 'SECRET_KEY=' and replaces it
            with the new value.

        Raises:
            FileNotFoundError: if the .env file
                is not found or cannot be read.
         """
        today = utils.datetime.datetime.today()
        if today.hour == 0 and today.minute == 0:
            self.SECRET_KEY = secrets.token_hex(32)
            with open('.env', 'r') as f:
                env_lines = f.readlines()
            for i, line in enumerate(env_lines):
                if line.startswith('SECRET_KEY='):
                    env_lines[i] = f'SECRET_KEY={self.SECRET_KEY}\n'
                    break
            with open('.env', 'w') as f:
                f.writelines(env_lines)


@lru_cache
def application_setting():
    return ApplicationSetting()


@lru_cache
def database_setting():
    return DataBaseSetting()


@lru_cache
def auth_setting():
    return AuthSetting()
