"""
Absctraction repository for password
"""
from abc import ABC, abstractmethod


class PasswordRepository(ABC):
    """Abstract base class for an Password repository."""
    @abstractmethod
    def cctx(self):
        raise NotImplementedError

    @abstractmethod
    def crypto_context(self):
        raise NotImplementedError

    @abstractmethod
    def hash_password(self):
        raise NotImplementedError

    @abstractmethod
    def verify_password(self):
        raise NotImplementedError
