"""
Absctraction repository for Category
"""
from abc import ABC, abstractmethod


class CategoryRepository(ABC):
    """Abstract base class for an Category repository."""
    @abstractmethod
    def create_category(self):
        raise NotImplementedError

    @abstractmethod
    def get_category(self):
        raise NotImplementedError

    @abstractmethod
    def get_categories_by_user(self):
        raise NotImplementedError

    @abstractmethod
    def update_category(self):
        raise NotImplementedError

    @abstractmethod
    def delete_category(self):
        raise NotImplementedError
