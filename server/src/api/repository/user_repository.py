"""
Absctraction repository for User
"""
from abc import ABC, abstractmethod
from src.models import User


class UserRepository(ABC):
    """Abstract base class for an User repository."""

    @abstractmethod
    def create_user(self) -> User:
        raise NotImplementedError

    @abstractmethod
    def get_user_by_email(self) -> User:
        raise NotImplementedError

    @abstractmethod
    def get_user_by_id(self) -> User:
        raise NotImplementedError

    @abstractmethod
    def user_last_login(self) -> User:
        raise NotImplementedError
