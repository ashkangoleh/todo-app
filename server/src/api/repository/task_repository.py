"""
Absctraction repository for Task
"""
from abc import ABC, abstractmethod
from src.models import Task


class TaskRepository(ABC):
    """Abstract base class for an Task repository."""

    @abstractmethod
    def create_task(self) -> Task:
        raise NotImplementedError

    @abstractmethod
    def get_task_by_id(self, task_id: int) -> Task:
        raise NotImplementedError

    @abstractmethod
    def get_all_task_by_user(self, user_id: int) -> Task:
        raise NotImplementedError

    @abstractmethod
    def delete_task_by_id(self, task_id: int) -> Task:
        raise NotImplementedError

    @abstractmethod
    def update_task_by_id(self, task_id: int) -> Task:
        raise NotImplementedError

    @abstractmethod
    def get_task_by_completed(self, completed_task: bool) -> Task:
        raise NotImplementedError
