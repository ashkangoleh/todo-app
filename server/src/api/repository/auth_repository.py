"""
Absctraction repository for authentication
"""
from abc import ABC, abstractmethod


class AuthRepository(ABC):
    """Abstract base class for an authentication repository."""
    @abstractmethod
    def generate_tokens(self):
        raise NotImplementedError

    @abstractmethod
    def _generate_access_token(self):
        raise NotImplementedError

    @abstractmethod
    def _generate_refresh_token(self):
        raise NotImplementedError

    @abstractmethod
    def decode_token(self):
        raise NotImplementedError

    @abstractmethod
    def get_current_user(self):
        raise NotImplementedError
