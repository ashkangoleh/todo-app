"""
Task Service for managing tasks associated with category and user
"""
from src.api.schemas import TaskCreate, TaskUpdate, parse_obj_as
from src.db import Session, select, and_, or_
from src.models import Task
from src.api.services.category_service import CategoryService
from src.api.repository import TaskRepository
from src import utils


class TaskService(TaskRepository):
    """
    This class provides services for managing tasks
        associated with a specific category and user.

    Args:
    -----------------
    db (Session):
        The database session.

    user_id (int):
        The ID of the user associated with the tasks.

    category_id (int):
        The ID of the category associated with the tasks.


    Attributes:
    -----------------
    get_category:
        A property that retrieves the category associated with the tasks.

    Methods:
    -----------------
        create_task(task: TaskCreate) -> Union[Task, Dict[str, str]]:
            Creates a new task associated with the category and user.

        get_task_by_id(task_id: int):
            Retrieves a task by its ID, associated with the category and user.

        get_all_task_by_user():
            Retrieves all tasks associated with the category and user.

        delete_task_by_id(task_id: int):
            Deletes a task by its ID, associated with the category and user.

        update_task_by_id(task_id: int, task: TaskUpdate):
            Updates a task by its ID, associated with the category and user.

        get_task_by_completed():
            Retrieves all completed tasks
                associated with the category and user.

        search_task(search_for: str):
            Searches for tasks containing the specified search string,
            associated with the category and user.

    """

    def __init__(self, db: Session, user_id: int, category_id: int):
        self.db = db
        self.user_id = user_id
        self.category_id = category_id

    @property
    def get_category(self):
        category = CategoryService(
            self.db,
            user_id=self.user_id
        ).get_category(
            category_id=self.category_id
        )
        if not category:
            return None
        return category

    def create_task(
        self,
        task: TaskCreate
    ) -> utils.Union[Task, utils.Dict[str, str]]:
        if self.get_category is not None:
            task = {**task.dict()}
            task.update({
                "owner_id": self.get_category.user_id
            })
            stmt = Task(**task)
            self.db.add(stmt)
            self.db.commit()
            self.db.refresh(stmt)
            return stmt
        raise ValueError("Category not found.")

    def get_task_by_id(self, task_id: int):
        if self.get_category is not None:
            stmt = select(Task).where(
                and_(
                    Task.id == task_id,
                    Task.owner_id == self.user_id,
                    Task.category_id == self.get_category.id
                )
            )
            result = self.db.execute(
                stmt
            ).scalar()
            if not result:
                return None
            return result

    def get_all_task_by_user(self):
        if self.get_category is not None:
            stmt = select(Task).where(
                and_(
                    Task.owner_id == self.user_id,
                    Task.category_id == self.get_category.id
                )
            )
            result = self.db.execute(
                stmt
            ).scalars().all()
            return result

    def delete_task_by_id(
        self,
        task_id: int
    ):
        result = self.get_task_by_id(task_id=task_id)
        if not result:
            return None
        title_to_remove = result.title
        self.db.delete(result)
        self.db.commit()
        return {
            "status": "successfully",
            "msg": f"\'{title_to_remove}\' deleted."
        }

    def update_task_by_id(
        self,
        task_id: int,
        task: TaskUpdate
    ):
        if self.category_id is not None:
            try:
                task = {**task.dict()}
                task_to_update = self.get_task_by_id(
                    task_id=task_id
                )
                updated_task_data = {k: v for k,
                                     v in task.items()
                                     if k in TaskUpdate.__fields__
                                     }
                updated_task = parse_obj_as(
                    TaskUpdate,
                    updated_task_data
                )
                for field, value in updated_task.dict(
                    exclude_unset=True
                ).items():
                    setattr(
                        task_to_update,
                        field, value
                    )
                self.db.commit()
                self.db.refresh(task_to_update)
                return task_to_update
            except Exception:
                raise ValueError(
                    "Category not found or Body missing value"
                )
        return None

    def get_task_by_completed(self):
        if self.get_category is not None:
            stmt = select(Task).where(
                and_(
                    Task.completed,
                    Task.owner_id == self.user_id,
                    Task.category_id == self.get_category.id
                )
            )
            result = self.db.execute(
                stmt
            ).scalars().all()
            return result

    def search_task(self, search_for: str):
        if self.get_category is not None:
            stmt = select(Task).where(
                and_(
                    Task.owner_id == self.user_id,
                    Task.category_id == self.get_category.id,
                    or_(
                        Task.title.ilike(
                            "%{}%".format(search_for)
                        ),
                        Task.description.ilike(
                            "%{}%".format(search_for)
                        )
                    )
                )
            )
            result = self.db.execute(
                stmt
            ).scalars().all()
            return result
