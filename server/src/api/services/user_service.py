"""
User Service to Create Retrieve Update User
"""
from src.api.schemas.user_schema import UserCreate
from src.db import Session, select
from src.models import User
from src.api.repository import UserRepository
from src import utils
from src.api.exceptions import UserError
from src.api.auth import PasswordService


class UserService(UserRepository):
    """
    A class that provides methods to create,
        retrieve and update user information.

    Args:
    --------------
        db (Session): The database session.

    Methods:
    --------------
        create_user(user: UserCreate) -> Union[User, Dict[str, str]]:
            Creates a new user with the given information
                and returns the user object.

        get_user_by_email(email: str) -> User:
            Retrieves a user object from the database with
                the given email address.

        get_user_by_id(user_id: int) -> User:
            Retrieves a user object from the database with the given user ID.

        user_last_login(user_id: int) -> User:
            Updates the last login time for the user with the given
                ID and returns the updated user object.

        deactive_user_by_id(user_id: int) -> User:
            Move user to deactive

    """

    def __init__(self, db: Session):
        self.db = db

    def create_user(
        self,
        user: UserCreate
    ) -> utils.Union[User, utils.Dict[str, str]]:
        hashed_password = PasswordService(
            password=user.password).hash_password()
        db_user = self.get_user_by_email(user.email)
        if db_user:
            raise UserError(f"{user.email} already exists.")
        user = {**user.dict()}
        user.update({
            "password": hashed_password
        })
        user.pop('password2')
        db_user = User(**user)
        self.db.add(db_user)
        self.db.commit()
        self.db.refresh(db_user)
        return db_user

    def get_user_by_email(self, email: str) -> User:
        stmt = select(User).where(User.email == email)
        result = self.db.execute(stmt).one_or_none()
        if result is None:
            return None
        return result[0]

    def get_user_by_id(self, user_id: int) -> User:
        stmt = select(User).where(User.id == user_id)
        result = self.db.execute(stmt).one_or_none()
        if result is None:
            return None
        return result[0]

    def user_last_login(self, user_id: int) -> User:
        user = self.get_user_by_id(user_id=user_id)
        user.last_login = utils.datetime.datetime.now()
        self.db.commit()
        self.db.refresh(user)

    def deactive_user_by_id(self, user_id: int):
        user = self.get_user_by_id(user_id=user_id)
        user.is_active = False
        self.db.commit()
        self.db.refresh(user)
        return user
