"""
Category Service operations for CRUD functionality
"""
from src.models import Category
from src.api.repository import CategoryRepository
from src.db import Session, select, and_, or_, noload
from src.api.services.user_service import UserService
from fastapi.encoders import jsonable_encoder


class CategoryService(CategoryRepository, UserService):
    """This class provides operations for CRUD
        functionality for categories in a user's account.

    The `CategoryService` class extends the `CategoryRepository`
        and `UserService` classes to provide category-related functionality.

    Args:
    -------------
        db (Session):
            A SQLAlchemy session object representing a database session.

        user_id (int):
            An integer representing the ID of the user
                account associated with the instance.

    Methods:
    -------------
        create_category(name: str):
            Adds a new category to the user's account with the given name.
            Returns the newly created category object.

        get_categories_by_user():
            Returns a list of all categories associated
                with the user's account.

        get_category(category_id: int):
            Returns the category object with the given ID associated with the
                user's account. Raises a `ValueError`
                if the category does not exist.

        update_category(category_id: int, name: str):
            Updates the name of the category with the given ID associated with
             the user's account. Returns the updated category object.

        delete_category(category_id: int):
            Deletes the category with the given ID associated with the user's
             account. Returns a dictionary containing a success message.

        get_category_by_name(name:str):
            Returns the category object whose name matches the given string,
             associated with the user's account.

        search_category(search_for: str):
            Returns a list of category objects whose names contain the given
             search string and are associated with the user's account.


    Raises:
    ----------
        All methods in this class raise a `ValueError`
            exception if the user or category does not exist.
    """

    def __init__(self, db: Session, user_id: int = None) -> None:
        super().__init__(db=db)
        self.db = db
        self.user_id = user_id

    def create_category(self, name: str):
        user = self.get_user_by_id(user_id=self.user_id)
        if not user:
            raise ValueError("User not found.")
        category = Category(name=name, user_id=self.user_id)
        self.db.add(category)
        self.db.commit()
        self.db.refresh(category)
        return category

    def get_categories_by_user(self):
        stmt = select(Category).options(
            noload(Category.tasks)
        ).where(Category.user_id == self.user_id)
        result = self.db.execute(stmt).scalars().all()
        if not result:
            raise ValueError("Category not found")
        return result

    def get_all_categories(self):
        result = self.db.execute(select(Category).options(
            noload(Category.tasks)
        )).scalars().all()
        if not result:
            raise ValueError("Category not found")
        return result

    def get_category(self, category_id: int):
        category = self.db.execute(
            select(Category).where(
                Category.id == category_id,
                Category.user_id == self.user_id
            )
        ).scalar()
        return category

    def update_category(self, category_id: int, name: str):
        category = self.get_category(category_id=category_id)
        if not category:
            return None
        category.name = name
        self.db.commit()
        self.db.refresh(category)
        result = jsonable_encoder(category, exclude={
            "user_id", "id"
        })
        return result

    def delete_category(self, category_id: int):
        category = self.get_category(category_id=category_id)
        if not category:
            return None
        category_name_to_delete = category.name
        self.db.delete(category)
        self.db.commit()
        return {
            "status": "successfully",
            "msg": f"\'{category_name_to_delete}\' deleted."
        }

    def get_category_by_name(self, name: str):
        stmt = select(Category).where(Category.name.icontains(name))
        result = self.db.execute(stmt).one_or_none()
        return result

    def search_category(self, search_for: str):
        stmt = select(Category).where(
            and_(
                Category.user_id == self.user_id,
                or_(
                    Category.name.ilike("%{}%".format(search_for))
                )
            )
        )
        result = self.db.execute(stmt).scalars().all()
        return result
