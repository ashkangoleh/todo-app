from .authentication.user_route import user_route # noqa
from .authentication.auth_route import auth_route # noqa
from .task.task_route import task_route # noqa
from .category.category_route import category_route # noqa
