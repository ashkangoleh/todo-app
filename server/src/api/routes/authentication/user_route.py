"""
API route for user create.
"""
from fastapi import APIRouter, Depends
from starlette.exceptions import HTTPException
from src.api.schemas import UserCreate, UserInDBBase
from src.db import get_db, Session
from src.api.services import UserService
from src.api.exceptions import UserError

user_route = APIRouter(
    prefix="/api/v1/user",
    tags=['User']
)


@user_route.post('/create')
async def create_user(
    user: UserCreate, db: Session = Depends(get_db)
) -> UserInDBBase:
    """Create User.

    Parameters:
    -----------
    user : UserCreate (include (email and password))
        The User information to create.

    db : Session, optional
        The database session, by default Depends(get_db).

    Returns:
    --------
    JSONResponse
        The message of a created user.

    Raises:
    -------
    HTTPException
        If there's an error when the password or username is/are incorrect.
    """
    try:
        stmt = UserService(db=db).create_user(user=user)
        return UserInDBBase.from_orm(stmt)
    except UserError as e:
        raise HTTPException(
            detail=f"{e}",
            status_code=400
        )
