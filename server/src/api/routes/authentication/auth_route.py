"""
API routes for user authentication and authorization.
"""
from fastapi import APIRouter, Depends
from starlette.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from src.api.auth.auth_handler import authentication
from starlette.exceptions import HTTPException
from src.api.schemas import AuthBase
from src.db import Session, get_db
from src.api.services import UserService
from src import utils

auth_route = APIRouter(
    prefix="/api/v1/auth",
    tags=['Authentication']
)


@auth_route.post("/token")
async def login_for_tokens(
    form_data: AuthBase,
    db: Session = Depends(get_db)
) -> utils.Dict:
    """Login user by generate JWT token.

    Parameters:
    -----------
    form_data : AuthBase (include (email and password))
        The User information to generate JWT.

    db : Session, optional
        The database session, by default Depends(get_db).

    Returns:
    --------
    JSONResponse
        The message of a generated JWT.

    Raises:
    -------
    HTTPException
        If there's an error when the user does not exists.
    """
    user = authentication.authenticated_user(
        db=db, email=form_data.email, password=form_data.password)
    if not user:
        raise HTTPException(
            status_code=400,
            detail="User does not exists.",
            headers={
                "WWW-Authenticate": "Bearer"
            }
        )
    user = jsonable_encoder(user, exclude={'password'})
    tokens = authentication.generate_tokens(data=user)
    UserService(db=db).user_last_login(user_id=user['id'])
    return JSONResponse(
        content=tokens,
        status_code=200
    )
