"""
API routes for Category CRUD depends on User id.
"""
from fastapi import APIRouter, Depends, HTTPException, Security
from src.api.schemas import CategoryInDBBase
from src.api.auth.auth_handler import get_current_active_user, security_scheme
from src.db import Session, get_db
from src import utils
from src.api.schemas import CategoryCreate, CategoryUpdate
from src.api.services import CategoryService
from starlette.responses import JSONResponse


category_route = APIRouter(
    prefix="/api/v1/category",
    tags=['Category'],
    dependencies=[Security(security_scheme)]
)


@category_route.post("/")
async def create_category(
    category: CategoryCreate,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Create Category.

    Parameters:
    -----------
    category : CategoryCreate
        The task information to create.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of a created Category.

    Raises:
    -------
    HTTPException
        If there's an error when not authenticated.
    """
    try:
        stmt = CategoryService(
            db=db, user_id=user_id).create_category(category.name)
        return stmt
    except Exception as e:
        raise HTTPException(
            status_code=400,
            detail=f"{e}"
        )


@category_route.get("/categories",
                    response_model=utils.List[CategoryInDBBase]
                    )
async def get_categories(
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Get all Category.

    Parameters:
    -----------
    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of Category/s as a list.

    Raises:
    -------
    HTTPException
        If there's an error when not authenticated.
    """
    try:
        db_categories = CategoryService(
            db=db, user_id=user_id).get_categories_by_user()
        return JSONResponse(
            content=utils.model_jsonable_decoder(db_categories),
            status_code=200
        )
    except Exception as e:
        raise HTTPException(
            detail=f"{e}",
            status_code=400
        )


@category_route.delete("/categories", response_model=utils.Dict)
def delete_category(
    category_id: int,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Delete the Category by category ID.

    Parameters:
    -----------
    category_id : int, optional
        The category ID, by default None.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of a deleted Category.

    Raises:
    -------
    HTTPException
        If there's an error when not authenticated.
    """

    db_categories = CategoryService(
        db=db, user_id=user_id).delete_category(category_id=category_id)
    if db_categories is None:
        raise HTTPException(
            detail="Category id/name not found.",
            status_code=403
        )
    return JSONResponse(
        content=db_categories,
        status_code=200
    )


@category_route.put("/categories", response_model=CategoryInDBBase)
def update_category(
    category: CategoryUpdate,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Update the Category by category ID.

    Parameters:
    -----------
    category : CategoryUpdate (category_id is inside)
        The task information to update.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of a updated Category.

    Raises:
    -------
    HTTPException
        If there's an error when not authenticated.
    """

    db_categories = CategoryService(
        db=db, user_id=user_id).update_category(
            category_id=category.category_id,
            name=category.name
    )
    if db_categories is None:
        raise HTTPException(
            detail="category id/name Not found.",
            status_code=404
        )
    return JSONResponse(
        content=db_categories,
        status_code=200
    )


@category_route.get("/categories/search",
                    response_model=utils.List[CategoryInDBBase]
                    )
def search_category(
    search_field: str = None,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Search Category by name.

    Parameters:
    -----------
    search_field : str
        The search_field to search.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of a searched category as a list.

    Raises:
    -------
    HTTPException
        If there's an error when not authenticated.
    """
    db_categories = CategoryService(
        db=db, user_id=user_id).search_category(search_for=search_field)
    if not db_categories:
        raise HTTPException(
            detail=f"\'{search_field}\' Not found.",
            status_code=404
        )
    return JSONResponse(
        content=utils.model_jsonable_decoder(db_categories),
        status_code=200
    )


@category_route.get("/categories/all",
                    response_model=utils.List[CategoryInDBBase]
                    )
def get_all_existing_categories(db: Session = Depends(get_db)):
    """Get all existing Categories.

    Parameters:
    -----------
    db : Session, optional
        The database session, by default Depends(get_db).

    Returns:
    --------
    JSONResponse
        The message of all categories as a list.

    Raises:
    -------
    HTTPException
        If there's an error when categories does not exist.
    """
    try:
        db_categories = CategoryService(db=db).get_all_categories()
        return JSONResponse(
            content=utils.model_jsonable_decoder(db_categories),
            status_code=200
        )
    except Exception as e:
        raise HTTPException(
            detail=f"{e}",
            status_code=400
        )
