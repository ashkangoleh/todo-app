"""
API routes for Tasks CRUD depends on User ID and Category ID.
"""
from fastapi import APIRouter, Depends, Security
from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse
from src.api.auth.auth_handler import get_current_active_user, security_scheme
from src.api.services import TaskService
from src.api.schemas import TaskCreate, TaskInDB, TaskUpdateByIDs
from src.db import Session, get_db
from src import utils
task_route = APIRouter(
    prefix="/api/v1/task",
    tags=['Tasks'],
    dependencies=[Depends(security_scheme)]
)


@task_route.post('')
async def create_task(
    task: TaskCreate,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Create Task by category ID.

    Parameters:
    -----------
    task : TaskCreate
        The task information to create.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of a created Task.

    Raises:
    -------
    HTTPException
        If there's an error when category not found.
    """
    try:
        stmt = TaskService(db=db, user_id=user_id,
                           category_id=task.category_id
                           ).create_task(
                               task=task
        )
        return TaskInDB.from_orm(stmt)
    except Exception as e:
        raise HTTPException(
            status_code=400,
            detail=f"{e}"
        )


@task_route.get('')
async def get_task(
    task_id: int = None,
    category_id: int = None,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Get a task by Category ID and Task ID.

    Parameters:
    -----------
    task_id : int
        The task ID.

    category_id : int, optional
        The category ID, by default None.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of Task/s as a list.

    Raises:
    -------
    HTTPException
        If there's an error when category not found.
    """

    if task_id is None and category_id is None:
        raise HTTPException(
            status_code=400,
            detail="task_id/category_id parameters are required."
        )
    stmt = TaskService(
        db=db,
        user_id=user_id,
        category_id=category_id
    ).get_task_by_id(
        task_id=task_id
    )
    if not stmt:
        raise HTTPException(
            status_code=404, detail="Not found.")
    return TaskInDB.from_orm(stmt)


@task_route.get('/all')
async def get_all_tasks(
    category_id: int = None,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Get all task/s by Category ID.

    Parameters:
    -----------
    category_id : int
        The category ID, by default None.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of Task/s as a list.

    Raises:
    -------
    HTTPException
        If there's an error when category not found.
    """
    stmt = TaskService(
        db=db,
        user_id=user_id,
        category_id=category_id
    ).get_all_task_by_user()
    if not stmt:
        raise HTTPException(
            status_code=404,
            detail="Not Found."
        )
    return stmt


@task_route.delete('')
async def delete_task(
    task_id: int,
    category_id: int = None,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Delete a task by ID.

    Parameters:
    -----------
    task_id : int
        The task ID.

    category_id : int
        The category ID, by default None.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security(get_current_active_user).

    Returns:
    --------
    JSONResponse
        The message of successful deletion.

    Raises:
    -------
    HTTPException
        If there's an error when deleting the task.
    """
    stmt = TaskService(
        db=db,
        user_id=user_id,
        category_id=category_id
    ).delete_task_by_id(
        task_id=task_id)
    if not stmt:
        raise HTTPException(
            status_code=404,
            detail="task_id/category_id not found."
        )
    return JSONResponse(
        content=stmt,
        status_code=200,
    )


@task_route.get('/completed')
async def get_task_completed(
    category_id: int = None,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Completed a task by completed status.

    Parameters:
    -----------
    category_id : int
        The category ID, by default None.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of Task/s as a list which is/are done.

    Raises:
    -------
    HTTPException
        If there's an error when category not found.
    """

    stmt = TaskService(
        db=db,
        user_id=user_id,
        category_id=category_id
    ).get_task_by_completed()
    if not stmt:
        raise HTTPException(
            status_code=404,
            detail="No completed Task found."
        )
    return JSONResponse(
        content=utils.model_jsonable_decoder(stmt),
        status_code=200
    )


@task_route.get("/search")
async def search_task(
    search_field: str = None,
    category_id: int = None,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Search task by Title and Description.

    Parameters:
    -----------
    search_field : str
        The search_field to search.

    category_id : int
        The category ID, by default None.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of a searched Task/s as a list

    Raises:
    -------
    HTTPException
        If there's an error when category not found
        or Search parameter is required.
    """

    if search_field is None:
        raise HTTPException(
            status_code=400,
            detail="Search parameter is required.")
    stmt = TaskService(
        db=db,
        user_id=user_id,
        category_id=category_id
    ).search_task(
        search_for=search_field
    )
    if not stmt:
        raise HTTPException(
            status_code=404,
            detail=f"\'{search_field}\' Not found in current Category/Task"
        )
    return JSONResponse(
        content=utils.model_jsonable_decoder(stmt),
        status_code=200
    )


@task_route.put("")
async def update_task(
    task: TaskUpdateByIDs,
    db: Session = Depends(get_db),
    user_id: int = Security(get_current_active_user)
):
    """Update the Task by category id.

    Parameters:
    -----------
    task : TaskUpdateByIDs
        The task information to update.

    db : Session, optional
        The database session, by default Depends(get_db).

    user_id : int, optional
        The user ID, by default Security

    Returns:
    --------
    JSONResponse
        The message of a Task as a list

    Raises:
    -------
    HTTPException
        If there's an error when category not found.
    """
    try:
        stmt = TaskService(
            db=db,
            user_id=user_id,
            category_id=task.category_id
        ).update_task_by_id(
            task_id=task.task_id,
            task=task
        )
        return TaskInDB.from_orm(stmt)
    except Exception as e:
        raise HTTPException(
            status_code=400,
            detail=f"{e}"
        )
