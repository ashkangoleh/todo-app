"""
Password Service to handle incoming passwords
verify and hashing password
"""
from passlib.context import CryptContext
from src.settings import AUTH_SETTING


class PasswordService:
    """
    PasswordService class for password hashing and verification.

    This class provides methods for hashing
        passwords and verifying hashed passwords using the passlib library.

    Attributes:
        _password (str): The password to be hashed or verified.

    Methods:
        crypto_context():
            Returns the CryptContext object for password hashing.

        cctx():
            Returns the CryptContext object for password hashing.

        hash_password():
            Hashes the password using the CryptContext object.

        verify_password(hashed_password: str):
            Verifies the password against a hashed password.
    """

    def __init__(self, password: str) -> None:
        self._password = password

    @classmethod
    def crypto_context(cls) -> CryptContext:
        return CryptContext(
            schemes=[AUTH_SETTING.PASSWORD_SCHEMES],
            deprecated="auto"
        )

    @property
    def cctx(self) -> CryptContext:
        return PasswordService.crypto_context()

    def hash_password(self) -> CryptContext:
        return self.cctx.hash(self._password)

    def verify_password(self, hashed_password: CryptContext) -> bool:
        return self.cctx.verify(self._password, hashed_password)
