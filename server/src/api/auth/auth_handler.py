"""
Auth handler for user authentication include
generate token (JWT), decode token (JWT), custom payload,
existing user, get user id, active user
"""
import uuid
from typing import Optional
from fastapi import Depends
from starlette.exceptions import HTTPException
from src.db import Session
from src.api.services import UserService, PasswordService
from src.settings import AUTH_SETTING
from src import utils
from jose import jwt, ExpiredSignatureError, JWTError
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials


security_scheme = HTTPBearer()


class Auth:
    """
    Auth class for handling authentication operations.

    This class provides methods for user authentication,
        token generation, and token decoding.

    Attributes:
        secret_key (str):
            The secret key used for token encryption.

        access_token_expiration (int):
            The expiration time for the access token in minutes.

        refresh_token_expiration (int):
            The expiration time for the refresh token in minutes.

        algorithm (str):
            The algorithm used for token encryption.

    Methods:
        authenticated_user(
            db: Session,
            email: str,
            password: str
            ) -> Union[bool, User]:
            Authenticates a user based on email and password.

        generate_tokens(data: Dict) -> Dict:
            Generates access and refresh tokens for a given data payload.

        payload(data: Dict, expiration_time: int) -> Dict:
            Generates the payload for a JWT token.

        _generate_access_token(data: Dict) -> str:
            Generates an access token using the payload.

        _generate_refresh_token(data: Dict) -> str:
            Generates a refresh token using the payload.

        decode_token(token: str) -> Optional[Dict]:
            Decodes and verifies a JWT token,
            returning the decoded token if valid.
    """

    def __init__(self):
        self.secret_key = AUTH_SETTING.SECRET_KEY
        self.ate = AUTH_SETTING.ACCESS_TOKEN_EXPIRE_MINUTES
        self.rte = AUTH_SETTING.REFRESH_TOKEN_EXPIRE_MINUTES
        self.algorithm = AUTH_SETTING.ALGORITHM

    @staticmethod
    def authenticated_user(db: Session, email: str, password: str):
        user = UserService(db=db).get_user_by_email(email=email)
        if not user:
            return False
        verify_password = PasswordService(
            password=password).verify_password(hashed_password=user.password)
        if not verify_password:
            return False
        return user

    def generate_tokens(self, data: utils.Dict) -> utils.Dict:
        access_token = self._generate_access_token(data)
        refresh_token = self._generate_refresh_token(data)
        return {
            'access_token': access_token,
            'refresh_token': refresh_token
        }

    @property
    def _uuid(self):
        return str(uuid.uuid4())

    def payload(self, data: utils.Dict, expiration_time: int) -> utils.Dict:
        return {
            'data': data,
            'exp': int(
                (utils.datetime.datetime.now()
                 + utils.datetime.timedelta(minutes=expiration_time)
                 ).timestamp()
            ),
            'jti': self._uuid
        }

    def _generate_access_token(self, data: utils.Dict) -> str:
        payload = self.payload(
            data=data, expiration_time=self.ate)
        access_token = jwt.encode(
            payload, self.secret_key, algorithm=self.algorithm)
        return access_token

    def _generate_refresh_token(self, data: utils.Dict) -> str:
        payload = self.payload(
            data=data, expiration_time=self.rte)
        refresh_token = jwt.encode(
            payload, self.secret_key, algorithm=self.algorithm)
        return refresh_token

    def decode_token(self, token: str) -> Optional[dict]:
        try:
            decoded_token = jwt.decode(
                token, self.secret_key, algorithms=[self.algorithm])
            return decoded_token
        except ExpiredSignatureError:
            return None
        except JWTError:
            return None


# instantiated Auth
authentication = Auth()


async def get_current_user(
    token: HTTPAuthorizationCredentials = Depends(security_scheme)
):
    """Get the current user based on the provided token.

    Parameters:
        token (HTTPAuthorizationCredentials):
            The HTTP authorization credentials.

    Returns:
        dict: The decoded token data representing the current user.

    Raises:
        HTTPException: If the credentials cannot be validated.
    """
    credential_exception = HTTPException(
        status_code=401,
        detail="Could not validate credentials",
        headers={
            "WWW-Authenticate": "Bearer"
        }
    )
    try:
        decoded_token = authentication.decode_token(token.credentials)
        if decoded_token is None:
            raise credential_exception
        return decoded_token.get('data')
    except JWTError:
        raise credential_exception


async def get_current_active_user(
    current_user: utils.Dict = Depends(get_current_user)
):
    """Get the current active user based on the provided current user data.

    Parameters:
        current_user (utils.Dict): The current user data.

    Returns:
        int: The ID of the current active user.

    Raises:
        HTTPException: If the current user is inactive.
    """
    if not current_user.get("is_active"):
        raise HTTPException(
            status_code=400,
            detail="Inactive user."
        )
    return current_user['id']
