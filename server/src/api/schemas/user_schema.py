"""
User Schemas for handle incoming requests
"""
from typing import Optional
from pydantic import BaseModel, EmailStr, validator


class UserBase(BaseModel):
    email: EmailStr


class AuthBase(UserBase):
    password: str


class UserCreate(UserBase):
    password: str
    password2: Optional[str]

    @validator('password2')
    def passwords_match(cls, v, values, **kwargs):
        if 'password' in values and v != values['password']:
            raise ValueError('passwords do not match')
        return v


class UserInDBBase(UserBase):
    id: int

    class Config:
        orm_mode = True
