"""
Task and category Schemas for handle incoming requests
"""
from pydantic import BaseModel, Field, parse_obj_as  # noqa
from .user_schema import UserInDBBase
from src import utils


class TaskBase(BaseModel):
    title: str = Field(max_length=255)
    description: str
    priority: int = Field(le=100, ge=0)
    completed: bool = Field(default=False)


class TaskCreate(TaskBase):
    category_id: int = Field(gt=0, default=1)


class TaskUpdate(TaskBase):
    pass


class TaskUpdateByIDs(TaskUpdate):
    category_id: int = Field(gt=0, default=1)
    task_id: int = Field(gt=0, default=1)


class TaskInDBBase(TaskBase):
    id: int
    created_at: utils.datetime.datetime

    class Config:
        orm_mode = True


class CategoryBase(BaseModel):
    name: str


class CategoryCreate(CategoryBase):
    pass


class CategoryUpdate(CategoryBase):
    category_id: int = Field(gt=0, default=1)


class CategoryInDBBase(CategoryBase):
    id: int

    class Config:
        orm_mode = True


class TaskInDB(TaskInDBBase):
    owner: UserInDBBase
    category: CategoryInDBBase


class TaskOutList(BaseModel):
    Tasks: utils.List[TaskInDB]


class CategoryInDB(CategoryInDBBase):
    owner: UserInDBBase
    Tasks: utils.List[TaskInDB]
