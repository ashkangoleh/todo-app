# ToDo list

**This is testing programming skills**

## Run application

```bash
docker-compose up -d
```
>Port 8080 exposed

Root path will redirect to `Documentaion` path for using swagger change path to `/docs`


#### Documentation path (`/documentation`)

![redoc](./img/documentaion.png)


#### Swagger path (`/docs`)

![redoc](./img/Swagger.png)

### Folder Structure

<details>

- [Root](https://gitlab.com/ashkangoleh/todo-app)
  - [server](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server)
    - [docker-compose.yaml](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/docker-compose.yaml)
    - [dockerfile](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/dockerfile)
    - [entrypoint.sh](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/entrypoint.sh)
    - [main.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/main.py)
    - [requirements.txt](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/requirements.txt)
    - [sample.env](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/sample.env)
    - [src](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src)
      - [api](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api)
        - [auth](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/auth)
          - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/auth/__init__.py)
          - [auth_handler.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/auth/auth_handler.py)
          - [password_service.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/auth/password_service.py)
        - [exceptions](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/exceptions)
          - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/exceptions/__init__.py)
          - [app_exceptions.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/exceptions/app_exceptions.py)
          - [service_exception.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/exceptions/service_exception.py)
        - [repository](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/repository)
          - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/repository/__init__.py)
          - [auth_repository.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/repository/auth_repository.py)
          - [category_repository.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/repository/category_repository.py)
          - [password_repository.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/repository/password_repository.py)
          - [task_repository.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/repository/task_repository.py)
                    - [user_repository.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/repository/user_repository.py)
        - [routes](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/routes)
          - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/routes/__init__.py)
          - [authentication](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/routes/authentication)
            - [auth_route.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/routes/authentication/auth_route.py)
            - [user_route.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/routes/authentication/user_route.py)
          - [category](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/routes/category)
            - [category_route.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/routes/category/category_route.py)
          - [task](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/routes/task)
            - [task_route.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/routes/task/task_route.py)
        - [schemas](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/schemas)
          - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/schemas/__init__.py)
          - [task_category_schema.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/schemas/task_category_schema.py)
          - [user_schema.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/schemas/user_schema.py)
        - [services](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/api/services)
          - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/services/__init__.py)
          - [category_service.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/services/category_service.py)
          - [task_service.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/services/task_service.py)
          - [user_service.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/api/services/user_service.py)
      - [db](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/db)
        - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/db/__init__.py)
        - [base.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/db/base.py)
          - [database.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/db/database.py)
        - [session_local.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/db/session_local.py)
      - [helpers](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/helpers)
        - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/helpers/__init__.py)
        - [custom_logger.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/helpers/custom_logger.py)
      - [models](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/models)
        - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/models/__init__.py)
        - [category.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/models/category.py)
        - [task.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/models/task.py)
        - [user.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/models/user.py)
      - [settings](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/settings)
        - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/settings/__init__.py)
        - [app_handlers.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/settings/app_handlers.py)
        - [configs.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/settings/configs.py)
        - [routers.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/settings/routers.py)
        - [tags_metadata.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/settings/tags_metadata.py)
      - [utils](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/src/utils)
        - [__init__.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/utils/__init__.py)
        - [model_decoder.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/src/utils/model_decoder.py)
    - [stdout.log](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/stdout.log)
    - [test](https://gitlab.com/ashkangoleh/todo-app/-/tree/develop/server/test)
      - [test_database.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/test/test_database.py)
      - [test_main.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/test/test_main.py)
      - [test_user_route.py](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/test/test_user_route.py)
    - [test_main.http](https://gitlab.com/ashkangoleh/todo-app/-/blob/develop/server/test/test_main.http)

</details>